message(STATUS "Configuring aeva package")

include(SuperbuildVersionMacros)
superbuild_configure_project_version(aeva)

set(aeva_extra_projects
  aeva
  aevasession
  aevaexampledata
)

set(_superbuild_default_aeva ON)

list(APPEND projects_with_plugins aevasession)
set(aevasession_plugin_files
  "${aevasession_plugin_dir}/aeva.session.xml")

# Project Configuration
set_property(GLOBAL PROPERTY aeva_REQUIRED_PROJECTS "aeva;aevasession;aevaexampledata;cmb;smtk")
set_property(GLOBAL PROPERTY aeva_EXCLUDE_PROJECTS "")
list(APPEND superbuild_extra_package_projects "${aeva_extra_projects}")

# Omit unused plugins
list(APPEND smtk_plugin_omit
  smtkDelaunayPlugin
  # Omit these to avoid unused mesh selections
  smtkMeshPlugin
  smtkPVMeshExtPlugin
  # Unused sesssion plugins
  smtkMeshSessionPlugin
  smtkOscillatorSessionPlugin
  smtkPolygonSessionPlugin
  smtkVTKSessionPlugin)
set_property(GLOBAL PROPERTY smtk_plugin_omit ${smtk_plugin_omit})

# Exclude all of the CMB plugins
set_property(GLOBAL PROPERTY cmb_plugin_omit "<all>")

# TODO: auto generate the bundle scripts
#include(CMBBundleMacros)
#cmb_generate_bundle(aeva)
include(CMBBundleMacros)
cmb_generate_package_bundle(aeva
  CPACK_NAME "AEVA"
  PACKAGE_NAME "aevaCMB"
  APPLICATIONS "aevaCMB"
  PACKAGE_VERSION aeva
  HAS_EXAMPLES
  )
