set(ENABLE_python2 OFF CACHE BOOL "")
set(ENABLE_python3 ON CACHE BOOL "")

if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "x86_64")
  set(CMAKE_OSX_DEPLOYMENT_TARGET "10.14" CACHE STRING "")
endif ()

include("${CMAKE_CURRENT_LIST_DIR}/configure_common.cmake")
