# Capstone is a private program to which Kitware developers have access. We
# provide logic for situating these programs into our superbuild for development
# purposes only. Nothing is bundled or distributed.
superbuild_add_project(capstone
  CAN_USE_SYSTEM
  CONFIGURE_COMMAND ""
  BUILD_COMMAND
    ${CMAKE_COMMAND} -E copy_directory "<SOURCE_DIR>" "<BINARY_DIR>"
  INSTALL_COMMAND ""
)

set(capstone_pythonpath ${CMAKE_CURRENT_BINARY_DIR}/capstone/build/bin)
set(CreateMG_DIR ${CMAKE_CURRENT_BINARY_DIR}/capstone/build/lib)
