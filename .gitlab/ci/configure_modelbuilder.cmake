# Capture the commit message for tagged releases.
if ("$ENV{CI_COMMIT_TITLE}" MATCHES "^modelbuilder: add release v\(.*\)$")
  set(modelbuilder_RELEASE "${CMAKE_MATCH_1}"
    CACHE STRING "")
endif ()

set(SUPERBUILD_PACKAGE_MODE "modelbuilder" CACHE STRING "")
set(ENABLE_cmb ON CACHE BOOL "")
set(ENABLE_opencascadesession ON CACHE BOOL "")
