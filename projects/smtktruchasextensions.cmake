set(smtktruchasextensions_extra_cmake_args)
if (UNIX)
  list(APPEND smtktruchasextensions_extra_cmake_args
    "-DPYTHON_EXECUTABLE:PATH=${superbuild_install_location}/bin/python${superbuild_python_version}")
endif ()

set(smtktruchasextensions_rpaths)
if (APPLE AND USE_SYSTEM_qt5)
  # On macOS, Qt5 packages use `@rpath` as their library ids. Add an rpath for
  # it to the build.
  list(APPEND smtktruchasextensions_rpaths
    "${qt5_rpath}")
endif ()

string(REPLACE ";" "${_superbuild_list_separator}"
  smtktruchasextensions_rpaths
  "${smtktruchasextensions_rpaths}")

set(python_module_dir "lib/python${superbuild_python_version}/site-packages")
if (WIN32)
  set(python_module_dir "bin/Lib/site-packages")
endif ()

set(enable_by_default OFF)
if ("${SUPERBUILD_PACKAGE_MODE}" STREQUAL "truchas")
  set(enable_by_default ON)
endif()

superbuild_add_project(smtktruchasextensions
  DEBUGGABLE
  DEFAULT_ON
  DEPENDS boost cxx11 paraview python qt5 smtk
  CMAKE_ARGS
    ${smtktruchasextensions_extra_cmake_args}
    -DBUILD_EXAMPLES:BOOL=OFF
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DCMAKE_INSTALL_RPATH:STRING=${smtktruchasextensions_rpaths}
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DPYBIND11_INSTALL:BOOL=${pybind11_enabled}
    -DSIMULATION_WORKFLOWS_ROOT:PATH=<INSTALL_DIR>/share/cmb/workflows
    -DSMTK_PYTHON_MODULEDIR:PATH=<INSTALL_DIR>/${python_module_dir}
    -DENABLE_PLUGIN_BY_DEFAULT=${enable_by_default}
  )

superbuild_declare_paraview_xml_files(smtktruchasextensions
  FILE_NAMES "smtk.truchas.xml"
  DIRECTORY_HINTS "smtk-${smtk_version}")
