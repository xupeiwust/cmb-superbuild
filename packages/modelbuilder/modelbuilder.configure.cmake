message(STATUS "Configuring modelbuilder package")
set_property(GLOBAL PROPERTY modelbuilder_REQUIRED_PROJECTS "cmb;smtk;cmbworkflows")
set_property(GLOBAL PROPERTY modelbuilder_EXCLUDE_PROJECTS "")

# Force this on for the modelbuilder mode
set(_superbuild_default_cmbworkflows ON)

set(modelbuilder_versions
  "6.3.0" "21.05.0")

set(modelbuilder_RELEASE "<development>"
  CACHE STRING "The version of CMB to build")
set_property(CACHE modelbuilder_RELEASE
  PROPERTY
    STRINGS "<development>;${modelbuilder_versions}")

if (modelbuilder_RELEASE STREQUAL "<development>")
  # Nothing special needed.
elseif (NOT modelbuilder_RELEASE IN_LIST modelbuilder_versions)
  message(FATAL_ERROR
    "Unsupported modelbuilder version ${modelbuilder_RELEASE}.")
elseif (modelbuilder_RELEASE STREQUAL "21.05.0")
  set(cmb_SOURCE_SELECTION "21.05.0")
  set(smtk_SOURCE_SELECTION "21.05.0")
  set(paraview_SOURCE_SELECTION "for-v21.05.0")
elseif (modelbuilder_RELEASE STREQUAL "6.3.0")
  set(cmb_SOURCE_SELECTION "6.3.0")
  set(smtk_SOURCE_SELECTION "3.3.0")
  set(paraview_SOURCE_SELECTION "for-v6.3.0")
else ()
  message(FATAL_ERROR
    "Unhandled modelbuilder version ${modelbuilder_RELEASE}.")
endif ()

include(CMBBundleMacros)
cmb_generate_package_bundle(modelbuilder
  HAS_EXAMPLES
  HAS_WORKFLOWS)
