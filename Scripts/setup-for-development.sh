#!/usr/bin/env bash

cd "${BASH_SOURCE%/*}/.." &&
Scripts/GitSetup/setup-user && echo &&
Scripts/GitSetup/setup-hooks && echo &&
Scripts/setup-git-aliases && echo &&
(Scripts/GitSetup/setup-upstream ||
 echo 'Failed to setup origin.  Run this again to retry.') && echo &&
(Scripts/GitSetup/setup-gitlab ||
 echo 'Failed to setup GitLab.  Run this again to retry.') && echo &&
Scripts/GitSetup/tips

# Rebase master by default
git config rebase.stat true
git config branch.master.rebase true

# Record the version of this setup so Scripts/pre-commit can check it.
SetupForDevelopment_VERSION=2
git config hooks.SetupForDevelopment ${SetupForDevelopment_VERSION}
