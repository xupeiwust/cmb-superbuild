superbuild_set_revision(cgm
  GIT_REPOSITORY "https://bitbucket.org/fathomteam/cgm.git"
  GIT_TAG 18403e5b100fe8d49a8ac0d6a2e849a0bbeae3bd)

superbuild_set_revision(meshkit
  GIT_REPOSITORY  "https://haocheng_liu@bitbucket.org/haocheng_liu/meshkit.git"
  GIT_TAG         osx-10-dot-13-patch)

superbuild_set_revision(moab
  GIT_REPOSITORY "https://bitbucket.org/fathomteam/moab.git"
  GIT_TAG 78d0b938fcf80fcdfb34fa39583fbce544b56953)
